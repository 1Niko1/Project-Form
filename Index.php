<?php
$conn = mysqli_connect("localhost","id18195605_scandiweb","Ntottralx1111*","id18195605_testtask");
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

if(isset($_POST['mass-delete-btn']) && isset($_POST['deleteCheck'])) {
    $skuArr = array();

    foreach (array_keys($_POST['deleteCheck']) as $sku) {
        $skuArr[] = "'$sku'";
    }

    $skuList = join(', ', $skuArr);

    $sql = "DELETE FROM product WHERE SKU IN($skuList)";
    mysqli_query($conn, $sql);
}

$sql = "SELECT SKU, product_name, Price, size, weight, height, width, length, product_type FROM product";
$products = mysqli_query($conn, $sql);

?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8"/>

    <!-- This is the scroll bar-->
    <style>
        ::-webkit-scrollbar {
            width: 10px;
        }

        ::-webkit-scrollbar-track {
            background: #f1f1f1;
        }

        ::-webkit-scrollbar-thumb {
            background: rgb(255, 207, 153);
        }

        ::-webkit-scrollbar-thumb:hover {
            background: rgb(255, 192, 127);
        }
    </style>

    <!-- These are the "ADD" & "MASS DELETE" buttons-->
    <style>
        .button {
            margin-top: auto;
            display: inline-flex;
            height: 25px;
            padding: 0;
            background-color: rgb(190, 190, 190);
            border: none;
            outline: none;
            border-radius: 7px;
            overflow: auto;
            font-family: 'Quicksand', sans-serif;
            font-size: 15px;
            font-weight: 400;
            cursor: pointer;
            text-align: center;
            position: relative;
            top: -50px;
            right: -86%;
            transition-duration: 0.5s;
        }

        .button:hover {
            background: rgb(134, 134, 134);
            box-shadow: 0 16px 24px 0 rgba(0, 0, 0, 0.2), 0 12px 20px 0 rgba(0, 0, 0, 0.19);
        }

        .button:active {
            background: rgb(158, 157, 157);
        }

        .button__text,
        .button__icon {
            display: inline-flex;
            align-items: center;
            padding: 0 3px;
            color: whitesmoke;
            height: 100%;
        }

        .button2 {
            display: inline-block;
            margin-top: auto;
            height: 25px;
            padding: 0;
            background-color: rgb(255, 0, 0);
            border: none;
            border-radius: 7px;
            overflow: hidden;
            font-family: 'Quicksand', sans-serif;
            font-size: 15px;
            font-weight: 400;
            cursor: pointer;
            text-align: center;
            position: relative;
            left: 86.3%;
            top: -45px;
            transition-duration: 0.5s;
        }

        .button2:hover {
            background: rgb(214, 33, 33);
            box-shadow: 0 8px 10px 0 rgba(255, 0, 0, 0.2), 0 6px 14px 0 rgba(255, 0, 0, 0.19);
        }

        .button2:active {
            background: rgb(158, 0, 0);
        }

        .button__text2,
        .button__icon2 {
            display: inline-flex;
            align-items: center;
            padding-top: 0 3px;
            float: left;
            margin-right: 4px;
            color: whitesmoke;
            height: 100%;
        }

    </style>

    <!--This is the Horizontal Rule-->
    <style>
        hr.one {
            height: 4px;
            border: 2px solid black;
            border-radius: 10px;
            background: linear-gradient(to left, blue, red);
            position: relative;
            top: -45px;
            margin-left: auto;
            margin-right: auto;
        }
    </style>

    <!--This is the Footer-->
    <style>
        footer {
            border-top: 2px solid rgb(0, 0, 0);
            position: RE;
            width: 100%;
            bottom: 0;
            background-color: rgb(255, 255, 255);
            color: rgb(0, 0, 0);
            text-align: center;
            margin-left: auto;
            margin-right: auto;
        }
    </style>

    <style>
        .border {
            width: 150px;
            border: 2px solid grey;
            border-radius: 5px;
            padding: 10px;
            text-align: center;
            margin: 20px;
            display: grid;
        }

    </style>

    <!-- Checkbox style -->
    <style>
        /* The container */
        .label-container {
            display: block;
            position: relative;
            padding-left: 10px;
            margin-bottom: 12px;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        /* Hide the browser's default checkbox */
        .label-container input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
            height: 0;
            width: 0;
        }

        /* Create a custom checkbox */
        .checkmark {
            position: absolute;
            top: 0;
            left: 0;
            height: 19px;
            width: 19px;
            background-color: #eee;
        }

        /* On mouse-over, add a grey background color */
        .label-container:hover input ~ .checkmark {
            background-color: #ccc;
        }

        /* When the checkbox is checked, add a blue background */
        .label-container input:checked ~ .checkmark {
            background-color: red;
        }

        /* Create the checkmark/indicator (hidden when not checked) */
        .checkmark:after {
            content: "";
            position: absolute;
            display: none;
        }

        /* Show the checkmark when checked */
        .label-container input:checked ~ .checkmark:after {
            display: block;
        }

        /* Style the checkmark/indicator */
        .label-container .checkmark:after {
            left: 6px;
            top: 1px;
            width: 5px;
            height: 10px;
            border: solid white;
            border-width: 0 3px 3px 0;
            -webkit-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            transform: rotate(45deg);
        }

        .grid-container {
            display: grid;
            grid-template-columns: auto auto auto auto;
        }

        .card {
            display: flex;
            justify-content: center;
            grid-row-gap: 30px;
        }

        .card .border {
            width: 100%;
        }

    </style>

    <script src="https://unpkg.com/ionicons@5.4.0/dist/ionicons.js"></script>
</head>

<body style="background-color: white;">

<h1 style="font-family:cursive; font-size: 2.5vw;">Product list</h1>

<a href="add.php" style="text-decoration: none" ;>
    <button type="button" class="button">
    <span class="button__icon">
        <ion-icon name="add-circle-outline"></ion-icon>
    </span>
        <span class="button__text">ADD</span>
    </button>
</a>

<button name="mass-delete-btn" form="delete-form" type="submit" class="button2">
    <span class="button__icon2">
        <ion-icon name="trash-outline"></ion-icon>
    </span>
    <span class="button__text2">MASS DELETE</span>
</button>

<hr class="one">

</a>

<form id="delete-form" action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post">
    <div class="grid-container">
        <?php while ($product = mysqli_fetch_assoc($products)) { ?>
            <div class="card">
                <p class="border">
                    <label class="label-container">
                        <input type="checkbox" name="deleteCheck[<?php echo $product['SKU']; ?>]">
                        <span class="checkmark"></span>
                    </label>
                    <?php
                    if ($product["product_type"] == 'DVD') {
                        echo $product["SKU"] . "<br>" . $product["product_name"] . "<br>" ."Price: ". $product["Price"] . "$" . "<br>" . "Size:" . $product["size"] . " MB" . "<br>";
                    } else if ($product["product_type"] == 'Book') {
                        echo $product["SKU"] . "<br>" . $product["product_name"] . "<br>" ."Price: ". $product["Price"] . "$" . "<br>" . "Weight: " . $product["weight"];
                    } else if ($product["product_type"] == 'Furniture') {
                        echo $product["SKU"] . "<br>" . $product["product_name"] . "<br>" . "Price: " . $product["Price"] . "$" . "<br>" . "Dimensions: " . $product["height"] . "x" . $product["width"] . "x" . $product["length"];
                    } ?></p>
            </div>
        <?php } ?>
    </div>
</form>


<footer>
    <p>Form project</p>
</footer>


</body>
</html>