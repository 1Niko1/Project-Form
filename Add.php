<?PHP
  $conn = mysqli_connect("localhost","id18195605_scandiweb","Ntottralx1111*","id18195605_testtask");
   mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
   
  $sku = $name = $price = $sizeinput = $weightinput = $heightinput = $widthinput = $lengthinput = $type = '';
  
  $errors = array();

  if($_SERVER['REQUEST_METHOD'] == 'POST') {

      //check sku
      if (empty($_POST['sku'])) {
          $errors['sku'] = 'Please, submit required data! <br>';
      } else {
          $sku = $_POST['sku'];
      }

      //check name
      if (empty($_POST['name'])) {
          $errors['name'] = 'Please, submit required data! <br>';
      } else {
          $name = $_POST['name'];
      }

      if (empty($_POST['price'])) {
          $errors['price'] = 'Please, submit required data! <br>';
      } else {
          $price = $_POST['price'];
      }

      if (empty($_POST['type'])) {
          $errors['type'] = 'Please, submit type! <br>';
      } else {
          $type = $_POST['type'];
      }
        
      if (empty($_POST['size'])) {
        $sizeinput = "NULL";
    } else {
        $sizeinput = $_POST['size'];
    }

    if (empty($_POST['weight'])) {
        $weightinput = "NULL";
    } else {
        $weightinput = $_POST['weight'];
    }

    if (empty($_POST['height'])) {
        $heightinput = "NULL";
    } else {
        $heightinput = $_POST['height'];
    }

    if (empty($_POST['width'])) {
        $widthinput = "NULL";
    } else {
        $widthinput = $_POST['width'];
    }

    if (empty($_POST['length'])) {
        $lengthinput = "NULL";
    } else {
        $lengthinput = $_POST['length'];
    }
    
      if (empty($errors)) {
          $sql = "INSERT INTO product (SKU, product_name, product_type, Price, size, weight, height, width, length) VALUES ('$sku','$name','$type',$price, $sizeinput ,$weightinput,$heightinput,$widthinput,$lengthinput)";
          if (array_filter($errors)) {
              echo 'errors in the form';
          } else {
              if ($result = mysqli_query($conn, $sql)) {
                  header('Location: index.php');
              }
          }
      }
  }

$conn ->close();
?>

<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8" />
    <link rel="stylesheet" href="index.css">

    <script src="https://unpkg.com/ionicons@5.4.0/dist/ionicons.js"></script>
</head>

<body style="background-color: white;" id="#product_form">

    <h1 style="font-family:cursive; font-size: 2.5vw;">Product Add</h1> 

    <a href= "index.php" style="text-decoration: none">
        <button type="button" class="button2" >
            <span class="button__icon2">
                <ion-icon name="close-outline"></ion-icon>
            </span>
            <span class="button__text2">Cancel</span>
        </button>
    </a>

    <hr class="one">

    <div>
        
        <form id="product_form" action="<?php $_SERVER['PHP_SELF'] ?>" method="post">
            
            <button class="button" name="submit">
                <span class="button__icon">
                    <ion-icon name="save-outline"></ion-icon>
                </span>
                <span class="button__text">Save</span>
            </button>

            <!--SKU:--><label style="font-family: sans-serif; position: relative; left:-66px; top:30px" id="sku">SKU</label>
            <input value="<?php echo htmlspecialchars($sku) ?>" style="position:relative; display: block; left:5.5%" type="text" name="sku" placeholder="Enter details"/>
            <div class="red-text"><?php echo $errors['sku'] ?? '';?></div>
            <br>
            
            <!--NAME:--><label style="font-family: sans-serif;" id="name">Name</label>
            <input value="<?php echo htmlspecialchars($name)?>" type="text" name="name"  placeholder="Enter details"/>
            <div class="red-text"><?php echo $errors['name'] ?? '';?></div>
            <br>
            
            <!--Price:--><label style="font-family: sans-serif;" id="price">Price ($)</label>
            <input value="<?php echo htmlspecialchars($price)?>" type="number" name="price" min="0" placeholder="Enter details" />
            <div class="red-text"><?php echo $errors['price'] ?? '';?></div>
            <br><br>

            
            <!--Type Switcher:--><label style="font-family: sans-serif;">Type Switcher</label>
            <select name="type" class="select" id="productType"> <!-- onchange='checktype(this.value)' -->
                <option value="" disabled selected>Type Switcher</option>
                <option value = "DVD">DVD-disc</option>
                <option value = "Book">Book</option>
                <option value = "Furniture">Furniture</option>
            </select>
            <div class="red-text"><?php echo $errors['type'] ?? '';?></div>
            <p id="type-error" style="color:red"></p>

            <div class="d-none" id="dvd-container">
                <p style="font-family: sans-serif;">Size:<p>
                <input type="number" name="size" id="sizeinput" style='display:block;'>
                <br><br>
            </div>

            <div class="d-none" id="book-container">
                <p style="font-family: sans-serif;">Weight:<p>
                <input type="number" name="weight" id="weightinput" style='display:block;'>
                <p style="font-family: sans-serif;">Please provide weight in Kg!</p>
                <br><br>
            </div>
            
            <div class="d-none" id="furniture-container">
                <p style="font-family: sans-serif;">Height:<p>
                <input type="number" name="height" id="heightinput" style='display:block;'>
                <br><br>

                <p style="font-family: sans-serif;">Width:<p>
                <input type="number" name="width" id="widthinput" style='display:block;'>
                <br><br>

                <p style="font-family: sans-serif;">Length:<p>
                <input type="number" name="length" id="lengthinput" style='display:block;' >
                <br><br>
            </div>
        </form>
    </div>

    <footer>
        <p>Scandiweb Test Assignment</p>
    </footer>

    <script src="index.js"></script>
</body>

</html>