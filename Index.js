const select = document.querySelector('#productType');

select.addEventListener('change', _ => {
  if(select.value == 'DVD'){
    document.querySelector('#dvd-container').classList.remove('d-none');

    document.querySelector('#book-container').classList.add('d-none');
    document.querySelector('#furniture-container').classList.add('d-none');
  }
  else if(select.value == 'Book'){
    document.querySelector('#book-container').classList.remove('d-none');

    document.querySelector('#dvd-container').classList.add('d-none');
    document.querySelector('#furniture-container').classList.add('d-none');
  }
  else{
    document.querySelector('#furniture-container').classList.remove('d-none');

    document.querySelector('#dvd-container').classList.add('d-none');
    document.querySelector('#book-container').classList.add('d-none');
  }
});

function formValidation(){
  if(!document.querySelector('#productType').value){
    document.querySelector('#type-error').textContent = 'Please, provide the data of indicated type';

    return false;
  }

  return true
}